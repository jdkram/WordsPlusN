DICTIONARY = File.readlines("dict84k.txt").map{|word| word.sub("\n","")} # Load the dictionary, trim the newlines

# Translate the original by n
def translate(original, n)
  original = original.downcase # lowercase the whole thing
  original = original.split(/\b/) # split on word boundaries 
  translated = original.collect do |word|
    index = DICTIONARY.index(word)
    if index.nil? || IGNORE_WORDS.include?(word)
      word
    else
      DICTIONARY[index+n]
      # TODO: add wraparound 
    end
  end
  return translated.join("")
end

# Translate a whole load at once, returning them as a big concatenated string
def translate_many(original,x,y)
    puts "Beginning translation loop from #{x} to #{y}"
    text = ""
    (x..y).each do |n|
    puts "Translating original by #{n}"
    text << "Jerusalem + #{n}\n\n"
    text << translate(original,n) + "\n\n--------------------\n\n"
  end
  return text
end

# Words that should remain untranslated
IGNORE_WORDS = %{
  and did bring me my in will
}

# Text to be translated
original = "And did those feet in ancient time,
Walk upon Englands mountains green:
And was the holy Lamb of God,
On Englands pleasant pastures seen!

And did the Countenance Divine,
Shine forth upon our clouded hills?
And was Jerusalem builded here,
Among these dark Satanic Mills?

Bring me my Bow of burning gold;
Bring me my Arrows of desire:
Bring me my Spear: O clouds unfold!
Bring me my Chariot of fire!

I will not cease from Mental Fight,
Nor shall my Sword sleep in my hand:
Till we have built Jerusalem,
In Englands green & pleasant Land."


open('translated10to110.txt', 'w') do |file|
  file.puts(translate_many(original,10,110))
end



# translate(original,12)
